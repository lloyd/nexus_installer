#### To install:
Requires conda, which can be found
https://conda.io/miniconda.html
and and internet connection

`bash install_nexus`

Nexus and all it's dependencies will be installed in a conda environment in  this folder

#### To run:
`./run_nexus`

##### Note:
Latter versions of conda break these tools